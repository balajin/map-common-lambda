MakeZip: yet another lambda deployment helper
=============================================

This submodule is intended to be used as a stand-alone console command.
It will generate a Lambda bundle from the code in current directory
and will upload it to the given Lambda function.

One important feature which is often missing in competing solutions
is an ability to include certain external files into the bundle,
i.e. files which are not Python code.
It is done with :code:`--extra` option.

.. for some reason, plain whitespace is dropped but %20 works

.. argparse::
   :ref: kasasa_common.makezip.make_parser
   :prog: makezip

.. automodule:: kasasa_common.makezip
