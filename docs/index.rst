.. DW Common Modules for Lambda documentation master file, created by
   sphinx-quickstart on Sun Jan 28 17:23:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Data Management Team!
==========================================================
Documentation for kasasa-common !
====

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   aws
   others
   makezip


TODO items
==========

.. todolist::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
