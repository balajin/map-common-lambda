AWS Helpers
===========

These modules provide helper classes which simplify using certain AWS services.
They are mostly based on `boto3` library but have simpler object-oriented interface.

S3
---

.. automodule:: kasasa_common.s3


STS
----

.. automodule:: kasasa_common.sts



SQS
---

.. automodule:: kasasa_common.sqs



SNS
---

.. automodule:: kasasa_common.sns

Lambda
----------

.. automodule:: kasasa_common.alambda

KMS
---

.. automodule:: kasasa_common.kms

X-Ray
---------

.. automodule:: kasasa_common.xray
