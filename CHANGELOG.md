04/25/2018
JIRA: DWT-2252, DWT-2348
bump the version to 3.0.0 since it a name change it is not backward compatible
update the doc strings
favor get_mysql_connection vs get_db_connection (deprecated)
add backports.csv to requirements
docstring typo on kasasa_common/sts.py
update .gitignore

1/3/2019
JIRA: REGDEV-273
Add data quality statistic to be shared among lambdas
Have sqs queue delete messages once they have been read