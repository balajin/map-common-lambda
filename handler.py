import json
import os.path as op
from traceback import print_exc

import common


def pig_script(event, context, upload=True):
    # parse parameters
    msg = json.loads(event['Records'][0]['Sns']['Message'])
    bucket = msg['Records'][0]['s3']['bucket']['name']
    key = msg['Records'][0]['s3']['object']['key']
    name = key.split('/', 1)[-1]
    name_without_ext = op.splitext(name)[0]

    # prepare pig
    with common.get_pig() as pig:
        pig.ensure_ready()

        # now do the job
        print('The bucket name is ' + bucket)
        print('The key is ' + key)
        print('The file name is ' + name)
        print('Running pig now...')

        print('1. Downloading file {} to /tmp/input/'.format(key))
        s3 = common.get_s3(bucket)
        s3.download(key, pig.INDIR)
        print('Finished downloading ' + name)

        print('2. Running pig...')
        pig.run(script=name_without_ext, outfile=name)

        print('Done running pig')
        # TODO print stdout and stderr?

        # now upload resulting file, unless in testing mode
        if upload:
            print('3. Uploading result...')
            s3.upload(op.join(pig.OUTDIR, name), 'post-transform/' + name)

if __name__ == '__main__':
    message = json.dumps({
        'Records': [
            {'s3': {
                'bucket': {'name': 'ez-map-transformation-dev-us-west-2'},
                'object': {'key': 'pre-transform/multi_output.txt'},
            }},
        ],
    })
    ret = pig_script(
        event={
            'Records': [
                {'Sns': {'Message': message}},
            ]
        },
        context=None,  # we don't use it anyway
    )
    print(ret)
