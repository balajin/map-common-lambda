# Common module interface for AWS-lambda related activities, such as 
pig transformation, S3 file transfers, SNS, KMS, etc.


Work in progress
API:
`get_pig`
	This method returns an instance of pig session helper class which has two primary methods:
	`ensure_ready` and `run`. Also it has a `cleanup` method which removes temporary directories after usage,
	or it can be used as a context manager.
	usage example : coming soon

`get_s3`
	Returns an instance of S3 helper for given bucket.
`get_db_connection`

`get_gpg`

`get_kms`

`kms_decrypt`



## Testing and deploying

### Preparing dev environment

Then you will want to use virtualenv. Create it as usual:

```
# for python3
python -m venv env
# or for python2
virtualenv env
# activate it
source env/bin/activate
# and then install dependencies
pip install -r requirements-test.txt
```

Requirements.txt includes main dependencies and also dev-specific ones.

## Testing

In order to run tests, just use `py.test` command. By default it will also measure coverage.
If you want coverage report in `html` format then use `--cov-report=html`.

## Deploying
Will follow the similar pattern as dw-python-modules

# Packages used:

## os:
- OS module in Python provides a way of using operating system dependent 
functionality. The functions that the OS module provides allows you to 
interface with the underlying operating system that Python is running on 
– be that Windows, Mac or Linux

## os.path:
- Common pathname manipulations

## sh: 
- is a full-fledged subprocess replacement for Python 2.6 - 3.6, PyPy and PyPy3 
that allows you to call any program as if it were a function

## tempfile:
- Handy dandy package to generate temporary files and directories

## time.sleep: --not using it anymore
- Provides various time-related functions
Suspend execution of the current thread for the given number of seconds. The
argument may be a floating point number to indicate a more precise sleep time. 

## glob:
- This module finds all the pathnames matching a specified pattern according to 
the rules used by the Unix shell, although results are returned in arbitrary order. 

## dotenv:
- Add .env support to your apps in development and deployments