#!/bin/sh

ARCHIVE=${1:-archive.zip}
PIG=${2:-pig-0.14.0.tar.gz}
# pass minus for pig name to not include pig at all
[ "$PIG" = "-" ] && PIG=

# make sure archive is not present
rm -f ${ARCHIVE}

# first make sure dependencies are installed
if ! [ -e env ]; then
	if which virtualenv > /dev/null; then
		virtualenv env
	else
		python -m venv env
	fi
	. env/bin/activate
	pip install -r requirements.txt
fi
# then package our code & pig
zip ${ARCHIVE} common.py handler.py ${PIG}
# now package dependencies
# except for boto3 which is pre-installed on lambda
cd env/lib/python*/site-packages
zip -r ../../../../${ARCHIVE} sh.py dotenv click --exclude '*.pyc'
