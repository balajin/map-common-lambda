def file_len(fname, bucket=None):
    i = -1  # defaults to -1 when empty file
    if bucket:
        with bucket.open_r(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1
    else:
        with open(fname, "rb") as f:
            for i, l in enumerate(f):
                pass
        return i + 1
