import os
import logging
from kasasa_common.vault import get_vault_secrets


logger = logging.getLogger('test_vault')


def test_get_vault_secrets():
    # arrange
    expected_results = dict(my_secret='kfc_recipe')
    os.environ['VAULT_APPROLE_PATH'] = 'infra-ops-s3-vault-dev-us-west-2/map-lambda-test-role-id'
    os.environ['VAULT_APPROLE_SECRET_ID'] = 'abc123'
    os.environ['VAULT_SECRET_KEY'] = 'map-lambda-test'
    os.environ['CONSUL_ADDRESS'] = 'http://0.0.0.0:8500'  # 'http://consul.dev.ksops.net:8500'
    os.environ['VAULT_PROTOCOL'] = 'http'

    # act
    actual_results = get_vault_secrets(logger)

    assert "my_secret" in actual_results
    assert actual_results["my_secret"] == expected_results["my_secret"]
