import datetime
import logging
import os
import random
import string

from kasasa_common.data_quality_statistic import DataQualityStatistic
import kasasa_common.tests.moto_helpers


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel('INFO')


def test_can_construct_data_quality_statistic_with_correct_attributes():
    dq_stats_row, fi_id, file_type, source = create_random_data_quality_statistic()
    assert dq_stats_row.fi_id == fi_id
    assert dq_stats_row.source == source
    assert dq_stats_row.file_type == file_type
    assert dq_stats_row.count == 0
    assert dq_stats_row.date_loaded == datetime.datetime.utcnow().strftime("%Y-%m-%d")
    assert dq_stats_row.stats_object == {}


def test_can_publish_data_quality_statistic_with_empty_inner_dq_stat_object(moto_sns_helper):
    os.environ["DQ_STAT_SNS_ARN"] = moto_sns_helper.topic.arn
    logger.info(os.environ["DQ_STAT_SNS_ARN"])

    dq_stat_to_publish, fi_id, file_type, source = create_random_data_quality_statistic()

    dq_stat_to_publish.publish_stat()
    dq_stat_messages = moto_sns_helper.messages
    assert len(dq_stat_messages) == 1
    assert dq_stat_messages[0] == {"count": 0,
                                   "source": source,
                                   "date_loaded": datetime.datetime.utcnow().strftime("%Y-%m-%d"),
                                   "file_type": file_type,
                                   "fi_id": fi_id, "stats_object": {}}


def test_can_publish_data_quality_statistic_with_duration_property(moto_sns_helper):
    os.environ["DQ_STAT_SNS_ARN"] = moto_sns_helper.topic.arn

    duration = datetime.timedelta(milliseconds=500)
    dq_stat_to_publish, fi_id, file_type, source = create_random_data_quality_statistic(duration)

    dq_stat_to_publish.publish_stat()
    dq_stat_messages = moto_sns_helper.messages
    assert len(dq_stat_messages) == 1
    assert dq_stat_messages[0] == {"count": 0,
                                   "source": source,
                                   "date_loaded": datetime.datetime.utcnow().strftime("%Y-%m-%d"),
                                   "file_type": file_type,
                                   "fi_id": fi_id, "stats_object": {"duration": '0:00:00.500000'}}


def create_random_data_quality_statistic(duration=None):
    fi_id = "99999"
    source = create_random_string(100)
    file_type = create_random_string(100)
    if duration is None:
        dq_stat_to_publish = DataQualityStatistic(fi_id, source, file_type)
    else:
        dq_stat_to_publish = DataQualityStatistic(fi_id, source, file_type, duration)
    return dq_stat_to_publish, fi_id, file_type, source


def create_random_string(length):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))
