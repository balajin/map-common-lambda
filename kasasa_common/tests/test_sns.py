import boto3
from botocore.exceptions import ClientError
import moto
import pytest

from kasasa_common.sns import get_sns, SNS
from kasasa_common.sqs import get_sqs


@pytest.fixture(autouse=True)
def moto_sns(moto_credentials):
    with moto.mock_sns():
        yield boto3.resource('sns')


@pytest.fixture
def topic(moto_sns):
    t = moto_sns.create_topic(Name='topic')
    return t


@pytest.fixture
def sns(topic):
    return get_sns(topic.arn)


# Use a queue to subscribe to the sns topic to ensure messages were sent correctly to the topic
@pytest.fixture(autouse=True)
def moto_sqs(moto_credentials):
    with moto.mock_sqs():
        yield boto3.resource('sqs')


@pytest.fixture
def queue(moto_sqs, topic):
    q = moto_sqs.create_queue(QueueName='test')
    q_arn = q.attributes.get('QueueArn')
    subscription = topic.subscribe(Protocol='sqs', Endpoint=q_arn)

    subscription.set_attributes(
        AttributeName="RawMessageDelivery",
        AttributeValue="true")
    return q


@pytest.fixture
def sqs(queue):
    return get_sqs(queue.url)


def test_send_message_with_attributes(sns, queue):
    import json
    message = "message"
    string_value = "string_value"
    int_value = 10
    float_value = 10.0
    message_attributes = {
        "string_value": string_value,
        "int_value": int_value,
        "float_value": float_value
    }

    sns.publish(message, message_attributes)

    # msgs = sqs.receive_all()
    msgs = queue.receive_messages(MaxNumberOfMessages=1, WaitTimeSeconds=2, MessageAttributeNames=['All'])
    body = json.loads(msgs[0].body)
    assert body["Message"] == message
    # It appears as though moto does not correctly send the message attributes.
    # assert msgs[0].message_attributes == message_attributes


