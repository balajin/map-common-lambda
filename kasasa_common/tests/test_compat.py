import os

import pytest

from .conftest import importextra
compat = importextra('compat')


def test_temporary_directory_available():
    # test that temporary directory is properly imported
    # regardless to python version
    assert compat.TemporaryDirectory
    with compat.TemporaryDirectory() as d:
        assert os.path.exists(d)
        assert os.path.isdir(d)
    assert not os.path.exists(d)


def test_rmtree_if_exists(tmpdir):
    a = tmpdir.join('a')
    abc = a.join('b', 'c')

    a.mkdir()
    a.join('b').mkdir()
    abc.mkdir()
    assert abc.exists()

    compat.rmtree_if_exists(a.strpath)

    assert not abc.exists()
    assert not a.exists()

    # it should not raise any error
    compat.rmtree_if_exists(a.strpath)

    assert not abc.exists()
    assert not a.exists()

    # and even for missing parent dirs
    compat.rmtree_if_exists(abc.strpath)

    # it should also handle files
    f = tmpdir.join('file')
    f.write('hello')

    compat.rmtree_if_exists(f.strpath)
    assert not f.exists()


def test_makedirs_unless_exist(tmpdir):
    # it should work well on both py2 and py3
    tgt = tmpdir.join('a', 'b', 'c')
    assert not tgt.exists()

    compat.makedirs_unless_exist(tgt.strpath)
    assert tgt.exists()
    assert tgt.isdir()

    # should not fail
    compat.makedirs_unless_exist(tgt.strpath)
    assert tgt.isdir()

    # should fail if a file exists with that name
    # so let's create such file
    tgt = tmpdir.join('f')
    with tgt.open('w'):
        pass
    assert tgt.exists()

    with pytest.raises(EnvironmentError):
        compat.makedirs_unless_exist(tgt.strpath)
