import os.path as op
import string

import py
import pytest

from .conftest import importextra
crypt = importextra('crypt')


@pytest.mark.skip(reason='WIP')
def test_get_gpg(mocker):
    mocker.patch.object(crypt, 'GpgHelper')

    g = crypt.get_gpg()
    assert isinstance(g, crypt.GpgHelper)
    crypt.GpgHelper.assert_called_with()

    g2 = crypt.get_gpg('test', conf='abc')
    assert isinstance(g, crypt.GpgHelper)
    assert g2 is not g
    crypt.GpgHelper.assert_called_with('test', conf='abc')


def _gpg():
    return crypt.GpgHelper(
        password='password',
    )

# key is in the same directory as tests are,
# so we use its absolute path as a bucket name
KEY_BUCKET = op.dirname(__file__)
PRIV = 'Key/kasasa_fico_private.key'
PUB = 'Key/kasasa_fico_public.key'


@pytest.fixture
def gpg():
    # new instance for each test
    return _gpg()


class TestGpgImportKeys(object):
    def test_s3_failure(self, gpg):
        # bucket path exists but has no key file
        with pytest.raises(Exception):
            gpg.import_keys_from_s3('/tmp', PRIV, PUB)

        # bucket does not exist
        with pytest.raises(ValueError):
            gpg.import_keys_from_s3('no-such-bucket', PRIV, PUB)

    def test_wrong_keyfile(self, gpg, tmpdir):
        for key in PRIV, PUB:
            tmpdir.join(key).write(
                'This pretends to be a key file but is not a correct one',
                ensure=True,
            )
        with pytest.raises(ValueError):
            gpg.import_keys_from_s3(tmpdir.strpath, PRIV, PUB)

    def test_wrong_password(self, gpg):
        gpg.password = 'not-a-valid-password'
        # it doesn't actually use the key itself (other than loading it),
        # so wrong password should not make any exception on this stage
        gpg.import_keys_from_s3(KEY_BUCKET, PRIV, PUB)


class TestObtainedKey(object):
    """ These tests consider key is already obtained,
    and for speed reason we don't want to re-obtain it each time.
    Hence different gpg fixture. """
    @pytest.fixture(scope='class')
    def gpg(self):
        gpg = _gpg()
        gpg.import_keys_from_s3(KEY_BUCKET, PRIV, PUB)
        return gpg

    @pytest.fixture(scope='class')
    def gpg_rt(self):
        """
        This gpg instance is suitable for round-trip testing
        because it uses the same key both for encryption and decryption.
        """
        gpg = _gpg()
        gpg.import_keys_from_s3(KEY_BUCKET, PRIV, PRIV)  # here is the catch
        return gpg

    def test_key_obtained(self, gpg):
        assert gpg.private_keyhash
        assert gpg.public_keyhash
        assert gpg.known_keys
        # "main" keyhashes should be listed amoung known keys
        assert gpg.private_keyhash in gpg.known_keys
        assert gpg.public_keyhash in gpg.known_keys

    def test_roundtrip(self, gpg_rt, tmpdir):
        gpg = gpg_rt

        content = 'Hello world!\nThis is test'
        src = tmpdir.join('original.txt')
        src.write(content)
        enc = tmpdir.join('encrypted.txt.gpg')
        tgt = tmpdir.join('decrypted.txt')

        gpg.encrypt_file(src.strpath, enc.strpath)
        gpg.decrypt_file(enc.strpath, tgt.strpath)

        encdata = enc.read_binary()
        # check that encrypted file is not just a copy&paste
        assert encdata != content
        # check that original text cannot be found in encrypted file
        assert content.encode() not in encdata

        assert tgt.read() == content

        # and we want to check that target file will be overwritten
        # even if already exists
        tgt.write('Something existing')
        gpg.decrypt_file(enc.strpath, tgt.strpath)
        assert tgt.read() == content

    def test_signature_checking(self, gpg_rt, tmpdir, monkeypatch):
        gpg = gpg_rt

        src = tmpdir.join('original.txt')
        src.write('Hello world')
        enc = tmpdir.join('encrypted.txt.gpg')
        tgt = tmpdir.join('decrypted.txt')

        gpg.encrypt_file(src.strpath, enc.strpath, sign=False)
        with pytest.raises(ValueError, match='not signed'):
            gpg.decrypt_file(enc.strpath, tgt.strpath, check_signature=True)
        # and signature checking should be the defaut behaviour
        with pytest.raises(ValueError, match='not signed'):
            gpg.decrypt_file(enc.strpath, tgt.strpath)

        # now encrypt it correctly
        gpg.encrypt_file(src.strpath, enc.strpath, sign=True)
        # and pretend we don't know this key's fingerprint
        monkeypatch.setattr(gpg, 'known_keys', [])
        with pytest.raises(ValueError, match='fingerprint.*not known'):
            gpg.decrypt_file(enc.strpath, tgt.strpath)

    def test_decrypting_wrong_data(self, gpg, tmpdir):
        enc = tmpdir.join('encrypted.txt.gpg')
        enc.write('This could be an encrypted file, but it isn\'t')
        tgt = tmpdir.join('decrypted.txt')

        with pytest.raises(ValueError, match='failed'):
            gpg.decrypt_file(enc.strpath, tgt.strpath)

    def test_armor(self, gpg_rt, tmpdir):
        gpg = gpg_rt

        data = 'Hello world\n'
        bdata = data.encode()
        src = tmpdir.join('original.txt')
        src.write(data)
        binary = tmpdir.join('binary.txt.gpg')
        armored = tmpdir.join('armored.txt.gpg')
        tgt = tmpdir.join('decrypted.txt')

        gpg.encrypt_file(src.strpath, binary.strpath, armor=False)
        gpg.encrypt_file(src.strpath, armored.strpath, armor=True)

        a = armored.read_binary()
        b = binary.read_binary()
        # make sure we work with bytestrings
        printable = string.printable.encode()
        # test that armored != non-armored and != original data
        assert a != b
        assert a != bdata
        assert b != bdata
        # all chars in armored should be printable
        assert all(c in printable for c in a)
        # at least one char in non-armored should be non-printable
        assert any(c not in printable for c in b)
        # armored version is larger than non-armored
        assert len(b) < len(a)

        # both these files should be successfully decrypted to the same result
        gpg.decrypt_file(binary.strpath, tgt.strpath)
        assert tgt.read() == data
        # Also we check that target file is overwritten
        gpg.decrypt_file(armored.strpath, tgt.strpath)
        assert tgt.read() == data

    def test_roundtrip_fails_when_keys_are_different(self, gpg, tmpdir):
        content = 'Hello world!\nThis is test'
        src = tmpdir.join('original.txt')
        src.write(content)
        enc = tmpdir.join('encrypted.txt.gpg')
        tgt = tmpdir.join('decrypted.txt')

        gpg.encrypt_file(src.strpath, enc.strpath)
        with pytest.raises(ValueError, match='Decryption failed'):
            # it should fail because our private and public keys are different
            # so after we encrypted file with FICO's public key,
            # we should not be able to decrypt it with OUR private key
            gpg.decrypt_file(enc.strpath, tgt.strpath)

def test_cleanup(gpg):
    assert gpg.home
    home = py.path.local(gpg.home)
    assert home.exists()
    assert home.isdir()
    home.join('somefile.txt').write('Some data')
    home.join('somedir', 'otherfile.txt').write('Other data', ensure=True)

    gpg.cleanup()
    assert not home.exists()

    # test subsequent call properly raises error
    with pytest.raises(AssertionError):
        gpg.cleanup()

    # test file handling won't work after deleting home
    with pytest.raises(Exception):
        gpg.import_keys_from_s3(KEY_BUCKET, PRIV, PUB)

    # this should never make error
    del gpg  # we consider __del__ is called here


def test_context_manager(gpg):
    assert gpg.home
    home = py.path.local(gpg.home)
    assert home.exists()
    assert home.isdir()

    with gpg as x:
        assert x is gpg
        assert home.exists()

    assert not home.exists()

    # this should never make error
    del gpg  # we consider __del__ is called here


def test_cleanup_in_ctx_mgr(gpg):
    home = gpg.home
    assert op.exists(home)

    with pytest.raises(AssertionError):
        with gpg:
            assert op.exists(home)
            gpg.cleanup()
            assert not op.exists(home)
        # at this point, an exception should be raised for double cleanup

    assert not op.exists(home)

    # this should never make error
    del gpg  # we consider __del__ is called here


def test_cleanup_on_deletion(gpg):
    home = gpg.home
    assert op.exists(home)
    # if we just `del gpg` then it won't be really deleted yet
    # because as a fixture it is still referenced from pytest internals
    # so let's go the simpler way
    gpg.__del__()
    assert not op.exists(home)
    # and it should be re-entrant, i.e. should not fail after any cleanup
    gpg.__del__()
    assert not op.exists(home)
