import shutil
import tempfile

import os
from kasasa_common.file_length import file_len
from kasasa_common import get_s3

content = "this is content \n"


def test_file_len_with_file():
    with tempfile.NamedTemporaryFile() as my_tmp_file:
        assert file_len(my_tmp_file.name) == 0
        with open(my_tmp_file.name, "wb") as write_to_file:
            write_to_file.writelines(content)
        assert file_len(my_tmp_file.name) == 1
        with open(my_tmp_file.name, "ab") as write_to_file:
            write_to_file.writelines([content, content, content])
        assert file_len(my_tmp_file.name) == 4


def test_file_len_with_bucket():
    folder_name = 'file_len_test'
    file_name = 's3.txt'
    os.mkdir(folder_name)
    bucket = get_s3(folder_name)
    with open(os.path.join(folder_name, file_name), "w+") as my_tmp_file:
        assert file_len(file_name, bucket) == 0
        my_tmp_file.write(content)

    assert file_len(file_name, bucket) == 1

    with open(os.path.join(folder_name, file_name), "w+") as my_tmp_file:
        assert file_len(file_name, bucket) == 0
        my_tmp_file.writelines([content, content, content])

    assert file_len(file_name, bucket) == 3
    shutil.rmtree(folder_name, ignore_errors=True)
