import base64
import logging
import os

import boto3


logger = logging.getLogger(__name__)


class KMS(object):
    """ Simple wrapper for AWS KMS service """
    def __init__(self):
        self.kms = boto3.client(
            'kms',
            region_name=os.environ.get('kms-region'))

    def decrypt_binary(self, blob):
        ret = self.kms.decrypt(CiphertextBlob=blob)
        if not ret.get('KeyId'):
            # probably not required as decrypt() will raise itself
            raise ValueError('Decryption failed?')
        return ret['Plaintext']

    def decrypt(self, b64blob):
        """
        Take blob encoded with base64, decode and then decrypt it
        """
        if os.environ.get('DEBUG_KMS_BYPASS'):
            return b64blob
        blob = base64.decodestring(b64blob)
        return self.decrypt_binary(blob)


def get_kms():
    return KMS()


# shorthand
def kms_decrypt(b64):
    return get_kms().decrypt(b64)
