import datetime
import json
import os
import logging

from kasasa_common import sns

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel('INFO')


class DataQualityStatistic(object):
    def __init__(self, fi_id, source, file_type, duration=None, custom_stat_object=None):
        self.source = source
        self.fi_id = fi_id
        self.file_type = file_type
        self.count = 0
        self.date_loaded = datetime.datetime.utcnow().strftime("%Y-%m-%d")
        self.stats_object = {}

        if duration is not None:
            self.stats_object["duration"] = str(duration)
        if custom_stat_object is not None:
            self.stats_object = custom_stat_object

    def __repr__(self):
        return json.dumps(dict(
            source=self.source,
            fi_id=self.fi_id,
            file_type=self.file_type,
            count=self.count,
            date_loaded=self.date_loaded,
            stats_object=self.stats_object
        ))

    def __str__(self):
        return json.dumps(dict(
            source=self.source,
            fi_id=self.fi_id,
            file_type=self.file_type,
            count=self.count,
            date_loaded=self.date_loaded,
            stats_object=self.stats_object
        ))

    def publish_stat(self):
        logger.info(self)
        if not os.environ.get("LOCAL_SNS", ""):
            dq_stat_sns = sns.get_sns(os.environ["DQ_STAT_SNS_ARN"])
            dq_stat_sns.publish(message=json.loads(str(self)))
