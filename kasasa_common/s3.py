"""
This module provides two main classes.
One of them, :class:`.RealS3`, represents an S3 bucket.
Another class, :class:`.LocalS3`, is primarily meant to be used for testing.
It provides the same interface as :class:`.RealS3`
but uses local directory as a backend.

The recommended method to obtain these classes
is with :func:`.get_s3` function.
It will first check if given bucket name can be resolved
to a local directory path (either relative or absolute).
If so then it will return a :class:`.LocalS3` instance.
Otherwise it will look for real S3 bucket with given name,
and raise an exception if one is not found.

In addition this module provides :func:`get_secrets` helper function
which allows convenient access to ``secrets.json`` file stored on S3 bucket.
"""

import io
import json
import logging
import os
import os.path as op
import shutil
import sys

import boto3

from .compat import makedirs_unless_exist, rmtree_if_exists, urlparse
from botocore.client import ClientError


logger = logging.getLogger(__name__)


class BucketNotAvailable(ValueError):
    pass


class ResponseWrapper(io.IOBase):
    # This is the wrapper around urllib3.HTTPResponse
    # to work-around an issue shazow/urllib3#1305.
    #
    # Here we decouple HTTPResponse's "closed" status from ours.
    #
    # FIXME drop this wrapper after shazow/urllib3#1305 is fixed

    def __init__(self, resp):
        self._resp = resp

    def close(self):
        self._resp.close()
        super(ResponseWrapper, self).close()

    def readable(self):
        return True

    def read(self, amt=None):
        if self._resp.closed:
            return b''
        return self._resp.read(amt)

    def readinto(self, b):
        val = self.read(len(b))
        if not val:
            return 0
        b[:len(val)] = val
        return len(val)


class BaseS3Helper(object):
    """
    Base class providing common interface for both `LocalS3` and `RealS3`.

    This object represents a single bucket.
    """
    def __init__(self, bucket):
        self.bucket = bucket

    def list(self, prefix=None, only_after=None):
        """
        Iterate over objects in this bucket, yielding their keys.
        Objects are returned in arbitrary order.

        Can also be accessed by iterating over the bucket::

            bucket = get_s3(bucket_name)
            for key in bucket:
                bucket.download(key, mydir)

        Alternatively accepts optional ``prefix``
        to yield only those objects which are in the certain "directory".

        :param str prefix:
            if specified then will only list objects
            which start with the given prefix.
            Note that unlike core S3 behaviour,
            here prefix is treated as a directory -
            i.e. ``path/to`` prefix will include ``path/to/file.txt``
            but will *not* include ``path/tommy.txt``
            or ``path/tommy/file.txt``.

        :param str only_after:
            skip objects until the given key is encountered.
            Will yield objects starting with the *next* one
            after the given key.
        """
        raise NotImplementedError

    def list_prefix(self, prefix):
        src = op.join(self.path, prefix)
        logger.info('src: %s', src)

        if not op.exists(self.path):
            raise ValueError('Bucket {!r} has no object {!r}'.format(
                self.bucket, prefix))
        for fname in os.listdir(src):
            yield fname

    def list_dirs(self, prefix=None):
        """
        While :meth:`list` will yield only keys (?),
        this one will list only directories -
        much like ``ls -d */`` command does.

        For the following bucket::

            dir1/file1
            dir1/file2
            dir2/file3
            dir2/file4
            file5

        it will yield ``dir1/`` and ``dir2/`` (with the trailing slash).

        :param str prefix:
            same as the same-named argument to :meth:`list`.
            Will only yield directories residing under the given path.
        """
        raise NotImplementedError

    def __iter__(self):
        return self.list()

    def _normalize(self, key):
        # cut off any leading slashes
        key = key.lstrip('/')
        # compress any runs of slashes
        key = '/'.join(p for p in key.split('/') if p)
        return key

    def exists(self, key):
        """
        Check if given ``key`` exists in this bucket.
        Only "files" are considered, not "directories".
        """
        return self._exists(self._normalize(key))

    def _exists(self, key):
        raise NotImplementedError

    def download(self, key, path):
        """
        Download object identified by ``key`` to given local ``path``

        :param str key: object's identifier, may contain slashes
        :param str path: local path to save the file to,
            either directory with trailing ``/`` or full filename.

            If this value has a trailing slash,
            it will be interpreted as a directory.
            In such case `key`'s path (if any) will be ignored
            and only filename will be used for downloaded file.

            If this value doesn't end with a slash
            then it is treated as a final location of the file,
            be it absolute or relative path.
        """
        if path.endswith(op.sep):
            path += op.basename(key)

        base = op.dirname(path)
        if not op.isdir(base):
            raise ValueError('Not exists or not a directory: {}'.format(base))

        return self._download(self._normalize(key), path)

    def _download(self, key, fullpath):
        raise NotImplementedError

    def fetch(self, key, binary=False):
        """
        Obtain given object and return it as a string/blob
        (depending on ``binary`` argument's value).

        On python2 will return `python2:str` for binary
        and `python2:unicode` for text file;
        on python3 will use `py3:bytes` for binary and `py3:str` for text.
        """
        with self.open_r(key, binary) as f:
            return f.read()

    def open_r(self, key, binary=False):
        """
        Open given key as a read-only file-like object.
        It should be :meth:`~io.IOBase.close`\ d after usage.
        Can also be used in a ``with`` statement.
        """
        return self._open_r(self._normalize(key), binary)

    def _open_r(self, key, binary):
        raise NotImplementedError

    def upload(self, localobj, name=None, overwrite=False):
        """
        Upload given local file or directory to the bucket,
        optionally under a different name.
        Also can accept file-like objects (in binary mode).

        :param localobj: it should be one of the following:

            * absolute or relative path to a local file;
            * absolute or relative path to a local directory;
            * file-like object opened in binary mode
              (i.e. which yields `python2:str` in python2
              or `py3:bytes` in python3).

        :param str name: optional target name to be used.
            It is mandatory when ``localobj`` is a file-like object
            and has no sensible ``name`` property.

        :param bool overwrite:
            by default we will refuse to overwrite already-existing object
            with the same name as the one being uploaded.
            This option allows to override that behaviour.

            .. note::
               Overwrite checking requires additional request to the server,
               so it might be useful to disable overwrite check for speed
               when appropriate by passing ``overwrite=True``.
        """
        if hasattr(localobj, 'read'):
            # it is a file-like object, handle it correspondingly
            if hasattr(localobj, 'mode') and 'b' not in localobj.mode:
                # non-binary mode won't work properly for RealS3,
                # especially in py3;
                # that is why we try to figure out such situations early
                raise ValueError('File should be opened in binary mode')

            localname = getattr(localobj, 'name', None)
            if not isinstance(localname, str) or localname == '<fdopen>':
                # for unnamed temporary files, name is either integer fd (py3)
                # or string <fdopen> (py2)
                localname = None  # in case we didn't detect all usecases
                if not name or name.endswith('/'):
                    # if name is not provided or is a directory
                    # then we need filename
                    raise ValueError('File object %r has no name attr, '
                                     'please provide name explicitly', localobj)
        else:
            # ensure no trailing slash
            localobj = localobj.rstrip(op.sep)
            if not op.exists(localobj):
                raise ValueError(
                    'No such file or directory: {}'.format(localobj))
            localname = localobj

        if not name:
            name = op.basename(localname)
        elif name.endswith(op.sep):
            name = op.join(name, op.basename(localname))
        name = self._normalize(name)

        if hasattr(localobj, 'read'):
            return self._upload_fileobj(localobj, name, overwrite)
        return self._upload(localobj, name, overwrite)

    def upload_with_args(self, localobj, name=None, overwrite=False, **kwargs):
        """
        Upload given local file or directory to the bucket,
        optionally under a different name.
        Also can accept file-like objects (in binary mode).

        :param localobj: it should be one of the following:

            * absolute or relative path to a local file;
            * absolute or relative path to a local directory;
            * file-like object opened in binary mode
              (i.e. which yields `python2:str` in python2
              or `py3:bytes` in python3).

        :param str name: optional target name to be used.
            It is mandatory when ``localobj`` is a file-like object
            and has no sensible ``name`` property.

        :param bool overwrite:
            by default we will refuse to overwrite already-existing object
            with the same name as the one being uploaded.
            This option allows to override that behaviour.

            .. note::
               Overwrite checking requires additional request to the server,
               so it might be useful to disable overwrite check for speed
               when appropriate by passing ``overwrite=True``.
        """
        if hasattr(localobj, 'read'):
            # it is a file-like object, handle it correspondingly
            if hasattr(localobj, 'mode') and 'b' not in localobj.mode:
                # non-binary mode won't work properly for RealS3,
                # especially in py3;
                # that is why we try to figure out such situations early
                raise ValueError('File should be opened in binary mode')

            localname = getattr(localobj, 'name', None)
            if not isinstance(localname, str) or localname == '<fdopen>':
                # for unnamed temporary files, name is either integer fd (py3)
                # or string <fdopen> (py2)
                localname = None  # in case we didn't detect all usecases
                if not name or name.endswith('/'):
                    # if name is not provided or is a directory
                    # then we need filename
                    raise ValueError('File object %r has no name attr, '
                                     'please provide name explicitly', localobj)
        else:
            # ensure no trailing slash
            localobj = localobj.rstrip(op.sep)
            if not op.exists(localobj):
                raise ValueError(
                    'No such file or directory: {}'.format(localobj))
            localname = localobj

        if not name:
            name = op.basename(localname)
        elif name.endswith(op.sep):
            name = op.join(name, op.basename(localname))
        name = self._normalize(name)

        if hasattr(localobj, 'read'):
            return self._upload_fileobj_with_args(localobj, name, overwrite, **kwargs)
        return self._upload_with_args(localobj, name, overwrite, **kwargs)

    def _upload(self, localobj, name, overwrite):
        raise NotImplementedError

    def _upload_with_args(self, localobj, name, overwrite, **kwargs):
        raise NotImplementedError

    def _upload_fileobj(self, localobj, name, overwrite):
        raise NotImplementedError
    
    def _upload_fileobj_with_args(self, localobj, name, overwrite, **kwargs):
        raise NotImplementedError

    def remove(self, key, recursive=False):
        """
        Delete an object denoted by given key.

        :param str key: either full name of the object to handle
            or a common prefix ("directory name").
        :param bool recursive: by default we won't delete directories.
            This argument should be set to `True` to allow that.
        """
        return self._remove(self._normalize(key), recursive)

    def _remove(self, key, recursive):
        raise NotImplementedError

    def rename(self, src, dst):
        """
        Rename object in-place, without downloading or uploading.
        This renaming is done within the same bucket only.

        :param str src: key of an existing object (not directory)
        :param str dst: target object name.
        """
        return self._rename(self._normalize(src), self._normalize(dst))

    def _rename(self, src, dst):
        raise NotImplementedError

    def copy_from(self, key, from_bucket=None, from_key=None):
        """
        Copy object from given bucket&key to this bucket.
        This method should usually be preferred over :meth:`copy_to`
        because it does not create `S3Helper` instance for destination bucket.
        An exception is when you want to use custom boto session
        with STS credentials.

        :param str key: target key
        :param from_bucket: (`str` or `BaseS3Helper`) --
            reference to the source bucket to copy from;
            defaults to this bucket if omitted.
        :param str from_key: source object's key;
            defaults to the same key as target one.
        """
        if not from_bucket and not from_key:
            raise ValueError(
                'At least one of (from_bucket, from_key) should be provided')

        if not from_bucket:
            from_bucket = self.bucket
        if isinstance(from_bucket, BaseS3Helper):
            # FIXME here we unwrap bucket name
            # but later in LocalS3 we wrap it again...
            from_bucket = from_bucket.bucket
        elif not from_key:
            from_key = key

        return self._copy_from(
            from_bucket,
            self._normalize(from_key),
            self._normalize(key),
        )

    def copy_from_with_args(self, key, from_bucket=None, from_key=None, **kwargs):
        """
        Copy object from given bucket&key to this bucket with additional key word arguments.
        This method should usually be preferred over :meth:`copy_to`
        because it does not create `S3Helper` instance for destination bucket.
        An exception is when you want to use custom boto session
        with STS credentials.

        :param str key: target key
        :param from_bucket: (`str` or `BaseS3Helper`) --
            reference to the source bucket to copy from;
            defaults to this bucket if omitted.
        :param str from_key: source object's key;
            defaults to the same key as target one.
        """
        if not from_bucket and not from_key:
            raise ValueError(
                'At least one of (from_bucket, from_key) should be provided')

        if not from_bucket:
            from_bucket = self.bucket
        if isinstance(from_bucket, BaseS3Helper):
            # FIXME here we unwrap bucket name
            # but later in LocalS3 we wrap it again...
            from_bucket = from_bucket.bucket
        elif not from_key:
            from_key = key

        return self._copy_from_with_args(
            from_bucket,
            self._normalize(from_key),
            self._normalize(key),
            **kwargs
        )

    def _copy_from(self, from_bucket, from_key, to_key):
        raise NotImplementedError

    def _copy_from_with_args(self, from_bucket, from_key, to_key, **kwargs):
        raise NotImplementedError

    def copy_to(self, key, to_bucket=None, to_key=None):
        """
        Copy object from this bucket to a different bucket and key.

        Usually you should prefer :meth:`copy_from`
        unless you need to use custom boto session which was
        set for the *source* bucket.

        Internally it will issue :meth:`copy_from` for the ``to_bucket``
        using the same boto session as the one used for this object,
        which is useful when copying stuff between AWS accounts.

        :param str key: key of an existing object (not directory)
        :param to_bucket: either name/path of the dest bucket
            or a `BaseS3Helper` subclass denoting target bucket.
            Defaults to current bucket.
            Should resolve to the same type (`LocalS3`, `RealS3`)
            as the current class.
        :param str to_key: key under which to write the objcet
            on target bucket.
            Defaults to original key.
        """
        if not to_bucket and not to_key:
            raise ValueError(
                'At least one of (to_bucket, to_key) should be provided')

        if to_bucket:
            if not isinstance(to_bucket, BaseS3Helper):
                # string name? wrap it with our class
                to_bucket = self._create_similar(bucket=to_bucket)
            elif not isinstance(to_bucket, type(self)):
                # ensure we are not trying to copy from local to real etc
                raise ValueError('Cannot copy between {} and {}'.format(
                    type(self),
                    type(to_bucket),
                ))

        return self._copy_to(
            self._normalize(key),
            to_bucket or self,
            self._normalize(to_key or key),
        )

    def copy_to_with_args(self, key, to_bucket=None, to_key=None, **kwargs):
        """
        Copy object from this bucket to a different bucket and key with additional key word arguments.

        Usually you should prefer :meth:`copy_from`
        unless you need to use custom boto session which was
        set for the *source* bucket.

        Internally it will issue :meth:`copy_from` for the ``to_bucket``
        using the same boto session as the one used for this object,
        which is useful when copying stuff between AWS accounts.

        :param str key: key of an existing object (not directory)
        :param to_bucket: either name/path of the dest bucket
            or a `BaseS3Helper` subclass denoting target bucket.
            Defaults to current bucket.
            Should resolve to the same type (`LocalS3`, `RealS3`)
            as the current class.
        :param str to_key: key under which to write the objcet
            on target bucket.
            Defaults to original key.
        """
        if not to_bucket and not to_key:
            raise ValueError(
                'At least one of (to_bucket, to_key) should be provided')

        if to_bucket:
            if not isinstance(to_bucket, BaseS3Helper):
                # string name? wrap it with our class
                to_bucket = self._create_similar(bucket=to_bucket)
            elif not isinstance(to_bucket, type(self)):
                # ensure we are not trying to copy from local to real etc
                raise ValueError('Cannot copy between {} and {}'.format(
                    type(self),
                    type(to_bucket),
                ))

        return self._copy_to_with_args(
            self._normalize(key),
            to_bucket or self,
            self._normalize(to_key or key),
            **kwargs
        )

    def _copy_to(self, src_key, dst_bucket, dst_key):
        raise NotImplementedError

    def _copy_to_with_args(self, src_key, dst_bucket, dst_key, **kwargs):
        raise NotImplementedError

    def _create_similar(self, bucket):
        """
        Internal method:
        create an instance of our type but for the different bucket.
        For RealS3 it will use the same session as the one used for self,
        thus retaining the same STS credentials
        (unlike just `type(self)(bucket)` which uses default account).
        """
        return type(self)(bucket)

    def __repr__(self):
        return '<{cname} bucket={bucket}>'.format(
            cname=self.__class__.__name__,
            bucket=self.bucket,
        )


class LocalS3(BaseS3Helper):
    """
    Implementation of s3 helper
    using local filesystem instead of real s3 bucket.
    It is expected to behave the same way as `RealS3`
    (although inconsistencies are possible).
    """
    def __init__(self, bucket):
        super(LocalS3, self).__init__(bucket)

        # try in current directory
        # then in app's base directory
        # the latter one is actually a fallback which might not work correctly

        # __file__ won't work when used from interactive python shell
        # so for it fall back to CWD
        base = getattr(sys.modules['__main__'], '__file__', '.')
        for path in [
            bucket,
            op.join(base, bucket),
        ]:
            if op.exists(path) and op.isdir(path):
                self.path = op.abspath(path)
                break
        else:
            raise BucketNotAvailable('No such local bucket: {}'.format(bucket))

    def list(self, prefix=None, only_after=None):
        if prefix:
            base = op.join(self.path, prefix.strip('/'))
            if not op.isdir(base):
                # Nothing to iterate over
                # (whether it is a file or missing)
                return
        else:
            base = self.path

        # we want to yield paths within the bucket,
        # hence cut based on our basepath
        cut = len(self.path) + 1

        for dirpath, dirnames, filenames in os.walk(base):
            for name in filenames:
                full = op.join(dirpath, name)
                # convert from full/relative path to key
                key = full[cut:]
                if only_after:
                    if key == only_after:
                        # start yielding from next key
                        only_after = None
                else:
                    yield key

    def list_prefix(self, prefix):
        src = op.join(self.path, prefix)
        logger.info('src: %s', src)

        if not op.exists(self.path):
            raise ValueError('Bucket {!r} has no object {!r}'.format(
                self.bucket, prefix))
        for fname in os.listdir(src):
            yield fname

    def list_dirs(self, prefix=None):
        base = self.path
        if prefix:
            base = op.join(base, prefix.strip('/'))
            if not op.isdir(base):
                # nothing to yield
                return

        cut = len(self.path) + 1
        for name in os.listdir(base):
            full = op.join(base, name)
            if op.isdir(full):
                yield full[cut:] + '/'

    def _exists(self, key):
        path = op.join(self.path, key)
        return op.exists(path) and not op.isdir(path)

    def _download(self, key, fullpath):
        src = op.join(self.path, key)
        if not op.exists(src):
            raise ValueError('Bucket {!r} has no object {!r}'.format(
                self.bucket, key))

        shutil.copyfile(src, fullpath)

    def _open_r(self, key, binary):
        return io.open(op.join(self.path, key), 'rb' if binary else 'r')

    def _upload(self, localobj, name, overwrite):
        subdir = op.dirname(name)
        if subdir:
            makedirs_unless_exist(op.join(self.path, subdir))

        src = localobj
        tgt = op.join(self.path, name)

        if overwrite:
            rmtree_if_exists(tgt)
        elif op.exists(tgt):
            raise ValueError('Already exists, won\'t overwrite')

        if op.isdir(localobj):
            shutil.copytree(src, tgt)
        else:
            shutil.copyfile(src, tgt)

    def _upload_with_args(self, localobj, name, overwrite, **kwargs):
        self._upload(localobj, name, overwrite)

    def _upload_fileobj(self, localobj, name, overwrite):
        tgt = op.join(self.path, name)

        if overwrite:
            rmtree_if_exists(tgt)
        elif op.exists(tgt):
            raise ValueError('Already exists, won\'t overwrite')

        # make sure all super directories exist
        makedirs_unless_exist(op.dirname(tgt))
        with open(tgt, 'wb') as tgtfile:
            while True:
                chunk = localobj.read(1024)
                if not chunk:
                    break
                tgtfile.write(chunk)

    def _upload_fileobj_with_args(self, localobj, name, overwrite, **kwargs):
        self._upload_fileobj(localobj, name, overwrite)

    def _remove(self, key, recursive=False):
        path = op.join(self.path, key)
        if not op.exists(path):
            raise ValueError('No such object: {}'.format(key))
        if recursive:
            shutil.rmtree(path)
        else:
            os.remove(path)
        # make sure any empty superdirs are removed
        # we cannot use removedirs here because it could remove `path` itself
        # list filtering is to avoid [''] as a result
        prefixdirs = [p for p in op.dirname(key).split(op.sep) if p]
        while prefixdirs:
            try:
                os.rmdir(op.join(self.path, *prefixdirs))
            except Exception:
                # probably non-empty
                break

    def _rename(self, src, dst):
        srcpath = op.join(self.path, src)
        dstpath = op.join(self.path, dst)

        # it will also make any required dirs and prune any old&empty ones
        os.renames(srcpath, dstpath)

    def _copy_from(self, src_bucket, src_key, dst_key):
        """
        :param src_bucket: either bucket of LocalS3 object.
            This is for optimization only.
        """
        if not isinstance(src_bucket, LocalS3):
            src_s3 = LocalS3(src_bucket)
        else:
            src_s3 = src_bucket
        srcpath = op.join(src_s3.path, src_key)
        dstpath = op.join(self.path, dst_key)

        basedir = op.dirname(dstpath)  # not the same as self.path
        if not op.isdir(basedir):
            os.makedirs(basedir)
        shutil.copyfile(srcpath, dstpath)

    def _copy_to(self, src_key, dst_s3, dst_key):
        return dst_s3._copy_from(self, src_key, dst_key)

    def _copy_to_with_args(self, src_key, dst_s3, dst_key, **kwargs):
        return dst_s3._copy_from(self, src_key, dst_key)

    def _copy_from_with_args(self, src_key, dst_s3, dst_key, **kwargs):
        return dst_s3._copy_from(self, src_key, dst_key)

    def __repr__(self):
        return '<LocalS3 bucket={bucket} path={path}>'.format(
            bucket=self.bucket,
            path=self.path,
        )


class RealS3(BaseS3Helper):
    """
    Implementation of S3 helper using real AWS S3 service.
    Can also work with `moto <https://github.com/spulec/moto>`_.
    """

    # extra parameters to pass to S3 when uploading files;
    # also used when copying.
    upload_extra_args = {
        'ServerSideEncryption': 'AES256',
    }

    def __init__(self, bucket, session=None, validate=True):
        """
        :param session: optional Boto3 session to use;
            can be obtained by :meth:`sts.get_sts_session`.
        :param bool validate: whether we want to check that such bucket exists.
            Can be set to `False` for optimization
            when one needs to create multiple helper instances
            for the same bucket which is known to exist.
        """
        super(RealS3, self).__init__(bucket)

        self.session = session
        self.S3 = (session or boto3).resource('s3')

        # check that bucket exists
        # there is no high-level way to check that, so use low-level client
        # https://stackoverflow.com/a/26871885/2267932
        try:
            if validate:
                self.S3.meta.client.head_bucket(Bucket=bucket)
        except ClientError as e:
            if e.response.get('Error', {}).get('Code') != '404':
                raise
            raise BucketNotAvailable(
                'Bucket does not exist or is not available: {}'.format(bucket))
        self.bucket_obj = self.S3.Bucket(bucket)

    def list(self, prefix=None, only_after=None):
        objects = self.bucket_obj.objects.all()
        if prefix:
            objects = objects.filter(
                # Important: we should end with a slash,
                # or else 'dir2' prefix will also list 'dir254/file.txt'
                Prefix=prefix.strip('/') + '/',
            )

        if only_after:
            # here we can just pass-through
            objects = objects.filter(
                Marker=only_after,
            )

        for obj in objects:
            yield obj.key

    def list_prefix(self, prefix):
        for obj in self.bucket_obj.objects.filter(Prefix=prefix):
            logger.info('list_prefix %s', obj.key)
            yield obj.key

    def list_dirs(self, prefix=None):
        if prefix:
            prefix = prefix.strip('/')
        else:
            prefix = ''  # None cannot be supported directly
        if prefix:
            prefix += '/'  # but for '' don't add it, and '/' gets ''

        # boto3 does not expose CommonPrefixes via "resource" API,
        # so we use underlying client directly.
        # https://stackoverflow.com/a/32674165/2267932
        paginator = self.bucket_obj.meta.client.get_paginator('list_objects')
        for result in paginator.paginate(
            Bucket=self.bucket,
            Delimiter='/',
            Prefix=prefix,
        ):
            for compref in result.get('CommonPrefixes', []):
                yield compref['Prefix']

    def _exists(self, key):
        # We don't want to download object's body,
        # so we use the same trick as in __init__:
        # call `client.head_object()` and check if it raises an exception.
        try:
            self.S3.meta.client.head_object(Bucket=self.bucket, Key=key)
            return True
        except ClientError as e:
            if e.response.get('Error', {}).get('Code') != '404':
                # TODO maybe handle something like "access denied"
                # as "existing" object?
                raise
            # 404 means object does not exist
            return False

    def _download(self, key, fullpath):
        # Important notice: unlike local filesystem,
        # S3 may have both path/to/file.txt and path/to files.
        # In such case we will do the following:
        # - if the key we were given ends with a slash then we fetch directory;
        # - else we fetch a file.

        # Looks like s3 first yields the shortest key first,
        # but we cannot be sure
        try:
            # let's pretend it is a file
            # (which is hopefully not the case if key ends with a slash)
            self.bucket_obj.download_file(key, fullpath)
        except ClientError:
            # it is probably not a file but directory (or nothing),
            # let's try to download it as a directory.
            # first, we don't want to fetch longdir/ contents
            # if we need `long` directory. So let's add a separator.
            if not key.endswith('/'):
                key += '/'
            cutchars = len(key)
            got = False
            for obj in self.bucket_obj.objects.filter(Prefix=key):
                got = True
                # obj is ObjectSummary instance
                targetpath = op.join(fullpath, obj.key[cutchars:])
                makedirs_unless_exist(op.dirname(targetpath))
                obj.Object().download_file(targetpath)
            if not got:
                raise ValueError('Key {} is neither file nor directory'.format(
                    key))

    def _open_r(self, key, binary):
        obj = self.bucket_obj.Object(key)
        ret = obj.get()
        body = ret['Body']
        # Originally, body returned is boto's wrapper
        # around urllib3's response.
        # Urllib3's one is compatible with io module and with stmt.
        # while the wrapper isn't.
        # The reason for that wrapper is to support timeouts
        # and check for incomplete reads;
        # we don't use timeouts and incomplete reads check is now implemented
        # in urllib3's version itself.
        body = body._raw_stream
        body.enforce_content_length = True  # default is false

        # now apply wrappers
        body = ResponseWrapper(body)  # this one is temporary, see docstring
        body = io.BufferedReader(body)
        if binary:
            return body  # use it as is
        return io.TextIOWrapper(body)

    def _upload_check_overwrite(self, fullpath):
        for obj in self.bucket_obj.objects.filter(Prefix=fullpath):
            # for fullpath='path/to', accept 'path/to' and 'path/to/file'
            # but not 'path/toast'
            if obj.key == fullpath or \
                    obj.key[len(fullpath):].startswith('/'):
                raise ValueError('Won\'t overwrite existing object')

    def _upload(self, localobj, fullpath, overwrite):
        if not overwrite:
            self._upload_check_overwrite(fullpath)

        if op.isfile(localobj):
            self.bucket_obj.upload_file(
                localobj, fullpath, ExtraArgs=self.upload_extra_args)
        elif op.isdir(localobj):
            for sub in os.listdir(localobj):
                self._upload(
                    op.join(localobj, sub),
                    op.join(fullpath, sub),
                    overwrite=True,  # as we should have already checked that
                )

    def _upload_with_args(self, localobj, fullpath, overwrite, **kwargs):
        if not overwrite:
            self._upload_check_overwrite(fullpath)

        if op.isfile(localobj):
            all_args = self.upload_extra_args.copy()
            all_args.update(dict(**kwargs))
            self.bucket_obj.upload_file(
                localobj, fullpath, ExtraArgs=all_args)
        elif op.isdir(localobj):
            for sub in os.listdir(localobj):
                self._upload_with_args(
                    op.join(localobj, sub),
                    op.join(fullpath, sub),
                    overwrite=True,  # as we should have already checked that
                    **kwargs
                )

    def _upload_fileobj(self, localobj, key, overwrite):
        if not overwrite:
            self._upload_check_overwrite(key)

        self.bucket_obj.upload_fileobj(
            localobj, key, ExtraArgs=self.upload_extra_args)

    def _upload_fileobj_with_args(self, localobj, key, overwrite, **kwargs):
        if not overwrite:
            self._upload_check_overwrite(key)

        all_args = self.upload_extra_args.copy()
        all_args.update(dict(**kwargs))

        self.bucket_obj.upload_fileobj(
            localobj, key, ExtraArgs=all_args)

    def _remove(self, key, recursive):
        if recursive:
            raise NotImplementedError(
                'Recursive deleting is not implemented yet')
        else:
            self.bucket_obj.Object(key).delete()
            # TODO check result

    def _rename(self, src, dst):
        # S3 has no rename API, so we will have to copy&delete:
        o = self.bucket_obj.Object(dst)
        # copy means "copy from whatever location to this object".
        # Unlike `copy_from` which is basically limited to 5GB,
        # this `copy` will use multiple threads if required
        # and its size limit is 5TB.
        # See https://docs.aws.amazon.com/AmazonS3/latest/API/RESTObjectCOPY.html
        o.copy(
            {'Bucket': self.bucket, 'Key': src},
            # as we are actually writing new object,
            # we will want to use upload args here as well.
            ExtraArgs=self.upload_extra_args,
        )
        # and now delete original file
        self.remove(src)

    def _copy_from(self, src_bucket, src_key, dst_key):
        dst_obj = self.bucket_obj.Object(dst_key)
        # copy means "copy from whatever location to this object"
        dst_obj.copy(
            {'Bucket': src_bucket, 'Key': src_key},
            ExtraArgs=self.upload_extra_args,
        )

    def _copy_from_with_args(self, src_bucket, src_key, dst_key, **kwargs):
        dst_obj = self.bucket_obj.Object(dst_key)
        # copy means "copy from whatever location to this object"
        all_args = self.upload_extra_args.copy()
        all_args.update(dict(**kwargs))
        dst_obj.copy(
            {'Bucket': src_bucket, 'Key': src_key},
            ExtraArgs=all_args,
        )

    def _copy_to(self, src_key, dst_s3, dst_key):
        return dst_s3._copy_from(self.bucket, src_key, dst_key)

    def _copy_to_with_args(self, src_key, dst_s3, dst_key, **kwargs):
        return dst_s3._copy_from_with_args(self.bucket, src_key, dst_key, **kwargs)

    def _create_similar(self, bucket):
        """
        Internal method:
        create an instance of our type but for the different bucket.
        For RealS3 it will use the same session as the one used for self,
        thus retaining the same STS credentials
        (unlike just `type(self)(bucket)` which uses default account).
        """
        # XXX we don't validate bucket name here
        # to speed things up.
        return RealS3(bucket, session=self.session, validate=False)


def get_s3(bucket, force_real=False, force_local=False, session=None, validate=False):
    """
    Return s3 helper class instance for given bucket id.
    """
    if all([force_real, force_local]):
        raise ValueError('Only one of (force_real, force_local) is allowed')

    if force_real:
        return RealS3(bucket, session)
    elif force_local:
        return LocalS3(bucket)

    # is requested bucket available locally?
    try:
        s3 = LocalS3(bucket)
        logger.debug('Using local directory %s instead of S3 bucket %s', s3.path, bucket)
    except BucketNotAvailable:
        # not available locally - try real s3 (might raise)
        s3 = RealS3(bucket, session, validate)
        logger.debug('Using actual AWS S3 instance for bucket %s', bucket)
    return s3

def get_secrets(bucket, key):
    """
    This helper function rovides cached access to secrets file.
    File is decoded from JSON.
    Upon first access it is cached in memory.

    Secrets file is a JSON-encoded text file
    containing passwords and other sensitive information
    which should not be stored in environment variables for some reason.
    """
    global _secrets
    if not _secrets:
        logger.debug('Obtaining secrets from S3 bucket %s, key %s...', bucket, key)
        s3 = get_s3(bucket)
        obj = s3.open_r(key)
        _secrets = json.load(obj)
    else:
        logger.debug('Using pre-fetched secrets...')
    return _secrets
_secrets = None

def get_bucket_and_key(s3_path):
    """
    GIVEN s3_path in the form s3://bucket/path/to/object
    WHEN get_bucket_and_key(s3_path)
    THEN return (bucket, /path/to/object)

    :param s3_path: The path to get the bucket and key from.
    :return: (bucket, path/to/object)
    """
    S3_SCHEME_NAME = "s3"
    parsed = urlparse.urlparse(s3_path)
    if parsed.scheme != S3_SCHEME_NAME:
        raise ValueError("{path} is not a valid s3 path".format(path=s3_path))
    bucket = parsed.netloc
    key = parsed.path
    return bucket, key
