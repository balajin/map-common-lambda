"""
This module contains skeleton implementations of several useful algorithms
which can be used as a base for lambda functions and other components.
"""

import json
import logging
import os

from dateutil.parser import parse as dateutil_parse
from kasasa_common import get_sqs, get_lambda, xray_optional
from kasasa_common.sqs import Message as SQSMessage


logger = logging.getLogger(__name__)


class BaseSQSFileProcessor(object):
    """
    Implements base algorithms for processing
    S3 objects based on SQS notifications.

    Usage: subclass it in your module and override some methods;
    then call desired entry points - either directly or from a function.

    Example::

        from kasasa_common.skel import BaseSQSFileProcessor


        class MyHandler(BaseSQSFileProcessor):
            def load_env(self):
                # important: don't forget to call super!
                super(MyHandler, self).load_env()

                # here you can load environment
                # and do other cheap preparations

            def can_process_file(self, bucket, key):
                # return True to process file (which is default)
                # or False to skip it.

            def process_file(self, bucket, key, timestamp):
                # Do whatever you want with the file.
                # For example, copy it somewhere.

    Now if you want to use it in a worker, call corresponding endpoint::

        if __name__ == '__main__':
            MyHandler.main_standalone()

    Or if you want to use it in lambda, expose desired entry point::

        def handle_one_by_one(event, context):
            return MyHandler.lambda_main_one_by_one(event, context)

    Or even simpler::

        handle_one_by_one = MyHandler.lambda_main_one_by_one  # no parentheses!

    (Lambda does not currently allow us
    to specify class method directly as an endpoint,
    hence we have to do such trick.)
    """

    def __init__(self):
        # initialize statistics dict
        # (for now it is used only in `main_standalone`)
        self.stats = dict(
            messages_total=0,
            messages_failed=0,
            files_total=0,
            files_success=0,
        )

    def load_env(self):
        """
        Load all required environment variables.
        This is implemented as a separate method which is called early,
        so that any missing variables will be detected early.

        Override this method if you want to load any additional settings
        and don't forget to call ``super(YourClass, self).load_env()``.

        It is important to not make any database connections here,
        because this method might be called even when we don't plan
        to do any real processing,
        e.g. when there are no messages
        or when current lambda is going to pass messages to another lambda.
        Hence it should be cheap but still do basic validation,
        like check if certain environment variable exists
        or if its value looks like a correct one.

        Default implementation recognizes the following environment variables:

        .. envvar:: QUEUE_URL

           URL of the SNS queue to receive messages from.

        .. envvar:: STOP_ON_ERRORS

           For main methods supporting that,
           settings this variable to nonempty value will request
           to stop handling after any message processing failed.
           Useful for debugging.

        .. envvar:: MESSAGES_COUNT

           For batch-handling main methods
           (like :meth:`lambda_main_one_by_one` and :meth:`lambda_main_gevent`)
           specifies how many messages should be processed in one batch.
           Defaults to ``10``.
        """
        self.queue_url = os.environ['QUEUE_URL']
        self.stop_on_errors = bool(os.environ.get('STOP_ON_ERRORS'))
        self.messages_count = int(os.environ.get('MESSAGES_COUNT', 10))

    def _stat_increment(self, key):
        self.stats[key] += 1

    def _stat_report(self):
        msg = ('Received total %(messages_total)d messages, '
               'failed to process %(messages_failed)d of them.')
        if self.stats['files_total']:
            # we don't want to try to report file stat
            # if our implementation does not use it at all,
            # like map-data-pipeline-output-livetech
            msg += (' Got information about %(files_total)d files, '
                    'and processed %(files_success)d of them.')
        logger.info(msg, dict(self.stats))

    def process_message(self, message):
        """
        Override this method if you want to implement
        general SQS message processing,
        not S3 file handling.

        This method is responsible for properly finalizing message
        after processing, i.e. delete or release it.
        The simplest way is to wrap most code in ``with message:`` block.

        :param kasasa_common.sqs.Message message: message object to process
        """
        logger.info('Start processing message %(msg)s', dict(msg=message))

        with message:
            payload_text = message.data.get('Message')
            assert payload_text, payload_text
            payload = json.loads(payload_text)

            self.process_message_payload(message, payload)

    def process_message_payload(self, message, payload):
        """
        Given message object and JSON-decoded body (message['Message']),
        do everything desired.
        This method should *not* delete message after processing,
        as it will be done by :meth:`process_message`.

        :param kasasa_common.sqs.Message message: message object
        :param dict,list payload: message payload
            JSON-decoded from message.body['Message'].
            It is usually a `dict`.
        """
        timestamp = None

        for record in payload['Records']:
            bucket = record['s3']['bucket']['name']
            key = record['s3']['object']['key']

            self._stat_increment('files_total')

            if not self.can_process_file(bucket, key):
                continue

            if not timestamp:
                # load it only once
                # and only if at least one file is suitable
                timestamp = dateutil_parse(message.data['Timestamp'])

            self.process_file(bucket, key, timestamp)

            self._stat_increment('files_success')

    def can_process_file(self, bucket, key):
        """
        Override this method if you want to skip processing some files.

        :returns: `True` to process the file (calling :meth:`process_file`),
            `False` otherwise.
        """
        return True

    def process_file(self, bucket, key, timestamp):
        """
        Implement your desired file handling logic here.
        This method will be called only for those files
        for which :meth:`.can_process_file` returned True.

        You should implement either this method
        or :meth:`process_message_payload`.

        :param str bucket: bucket where the file resides
        :param str key: object's key
        :param datetime timestamp: event's timestamp decoded to `datetime`.
        """
        raise NotImplementedError

    # Below are various entry points;
    # use whichever you like.

    @classmethod
    def lambda_main_one_by_one(cls, event, context):
        """
        Main method for Lambda function:
        when called will process up to :envvar:`MESSAGES_COUNT` messages
        (defaults to 10)
        and then re-launch itself again if there are more messages.
        The purpose is to handle all available messages at the end
        while not bumping into lambda's time limit.
        """
        self = cls()

        self.load_env()

        sqs = get_sqs(self.queue_url)

        last_exc = None
        for m in sqs.receive_max(self.messages_count):
            try:
                self.process_message(m)
            except Exception as e:
                logger.exception('Failed to handle message %s', m)
                last_exc = e

                if self.stop_on_errors:
                    raise

        if len(sqs) > 0:
            # there are some more messages;
            # invoke ourselves again (with no arguments) to handle them.
            logger.info('There are more messages remaining; '
                        'calling ourself again')
            get_lambda().invoke_async()

        if last_exc:
            # make sure Lambda knows there was a problem
            logger.error('There were errors handling some messages')
            raise last_exc

    @classmethod
    def lambda_main_gevent(cls, event, context):
        """
        Main method for Lambda function:
        when called will process up to :envvar:`MESSAGES_COUNT` messages
        in parallel using `gevent`,
        then re-launch itself again if there are more messages.
        Very similar to :meth:`lambda_main_one_by_one`
        but will try to process messages in parallel
        which might allow to increase MESSAGES_COUNT
        without bumping into limit.
        """
        # FIXME maybe move this func to separate module
        # to monkeypatch stuff before importing anything?
        import gevent
        from gevent import monkey
        monkey.patch_all()

        self = cls()
        self.load_env()

        sqs = get_sqs(self.queue_url)

        # first spawn all greenlets
        greenlets = []
        for m in sqs.receive_max(self.messages_count):
            g = gevent.spawn(self.process_message, m)
            # for logging purposes store msg data on greenlet object,
            # because there is no consistent method to access args
            g.msgdata = m.data  # for logging purposes,
            greenlets.append(g)
        # now wait for all of them to complete
        gevent.wait(greenlets)
        # and check results
        last_exc = None
        for g in greenlets:
            if g.exception:
                last_exc = g.exception
                logger.error(
                    'Processing message failed: %s', g.msgdata,
                    exc_info=g.exc_info,
                )

                if self.stop_on_errors:
                    raise

        if len(sqs) > 0:
            # there are some more messages;
            # invoke ourselves again (with no arguments) to handle them.
            logger.info('There are more messages remaining; '
                        'calling ourself again')
            get_lambda().invoke_async()

        if last_exc:
            # make sure Lambda knows there was a problem
            logger.error('There were errors handling some messages')
            raise last_exc

    @classmethod
    def lambda_main_with_sublambdas(cls, event, context):
        """
        Fetch all available messages from the queue
        and pass them to sub-lambda instances, one instance per message.

        Unlike other versions, this one won't stop on first failed message,
        instead it will try to process all of them.
        Also here it is not possible to know
        when all messages are finished processing.

        .. envvar:: SUBLAMBDA

            Should contain name or ARN of the sublambda
            capable of handling these messages.
            It should accept event of the following schema::

                {
                    "body": "JSON-encoded event body",
                    "handle": "message receipt handle used to delete it"
                }
        """
        self = cls()
        self.load_env()
        self.sublambda = os.environ['SUBLAMBDA']
        lam = get_lambda(self.sublambda)
        sqs = get_sqs(self.queue_url)

        for m in sqs.receive_all():
            lam.invoke_async({'body': m.body, 'handle': m.handle})

    @classmethod
    def lambda_main_sublambda(cls, event, context):
        """
        Main function for sublambda,
        required for :meth:`lambda_main_with_sublambdas`.
        Will expect message data in the event
        and will handle that message, raising an exception on failure.
        Otherwise it requires the same environment as the main lambda,
        except for :envvar:`QUEUE_URL`.
        """
        self = cls()
        self.load_env()

        # parse event
        msg_body = event['body']
        msg_handle = event['handle']
        # reconstruct SQS message
        sqs = get_sqs(self.queue_url)
        m = SQSMessage(sqs=sqs, msgid='unknown-msgid',
                       handle=msg_handle, body=msg_body)

        # process message (no need to try/catch as it is the only one);
        # exceptions will be logged automatically.
        self.process_message(m)

    @classmethod
    @xray_optional.within_segment('main')
    def main_standalone(cls):
        """
        Main method for standalone worker.
        Will process all available messages.
        Not to be used from within Lambda function.
        """
        self = cls()

        self.load_env()

        sqs = get_sqs(self.queue_url)

        last_exc = None
        for msg in sqs.receive_all():
            self._stat_increment('messages_total')
            try:
                self.process_message(msg)
            except Exception as e:
                self._stat_increment('messages_failed')
                # exception is already logged by msg's __exit__
                logger.exception('Failed to handle message %s', msg)
                last_exc = e

                if self.stop_on_errors:
                    raise

        self._stat_report()

        if last_exc:
            logger.error('There were errors handling some messages')
            raise last_exc
