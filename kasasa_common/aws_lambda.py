import base64
import json

import boto3


class Lambda(object):
    def __init__(self, name_or_arn):
        self.lam = boto3.client('lambda')
        # make sure name is valid;
        # if name/arn is inavlid then this call will raise
        # ResourceNotFoundException
        self.lam.get_function_configuration(FunctionName=name_or_arn)
        self.name = name_or_arn

    @staticmethod
    def __prepare_payload(payload):
        payload = json.dumps(payload)
        return payload.encode('utf-8')

    def invoke_async(self, **kwargs):
        self.lam.invoke(
            FunctionName=self.name,
            InvocationType='Event',
            Payload=self.__prepare_payload(kwargs),
        )

    def invoke_and_wait(self, **kwargs):
        """
        Launch specified function and wait for it to finish.
        If get_logs is True then will return tuple (result, logs) -
        notice that only last 4kb of logs are retrieved;
        else will return just result.
        On failure will raise an Exception.
        """
        # XXX this function is not tested yet
        get_logs = kwargs.pop('get_logs', False)
        # this will return only after function is done or failed
        result = self.lam.invoke(
            FunctionName=self.name,
            InvocationType='RequestResponse',
            LogType='Tail' if get_logs else 'None',
            Payload=self.__prepare_payload(kwargs),
        )
        r_payload = result['Payload'].read()
        r_payload = json.loads(r_payload)

        if get_logs:
            logs = base64.decodestring(result['LogResult'])
            return r_payload, logs
        return r_payload
