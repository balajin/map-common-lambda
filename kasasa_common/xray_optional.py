"""
This module conditionally provides some of :mod:`.xray` features,
namely :func:`.xray.capture`.
It is useful for library modules which want to provide xray subsegment tracking
when xray is active but still want to work without any problem
when xray is inactive and its requirements are not met.

When xray dependencies are not met at the time this module is imported,
it will provide an empty pass-through decorator for `.capture`.
"""
try:
    from .xray import capture, within_segment, get_trace_entity  # noqa
except ImportError:
    def capture(*args, **kwargs):
        """
        Empty pass-through decorator
        which replaces :func:`.xray.capture` when its requirements are not met.
        """
        if args and callable(args[0]):
            return args[0]
        return lambda fn: fn

    def within_segment(*args, **kwargs):
        """
        Empty pass-through decorator
        which replaces :func:`.xray.within_segment`
        when its requirements are not met.
        """
        # FIXME also support context manager syntax
        if args and callable(args[0]):
            return args[0]
        return lambda fn: fn

    def get_trace_entity():
        """
        There is no current trace entity.
        """
        return None
