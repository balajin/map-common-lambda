import logging
import shutil
import tempfile

import gnupg

from .s3 import get_s3


logger = logging.getLogger(__name__)


class GpgHelper(object):
    """ Wrapper around gnupg to simplify en/decrypting files """
    def __init__(self, password, sign=True, check_signature=True,
                 homedir=None):
        """
        :param password: password for private key
        :param sign: whether to sign encrypted files by default
        (can be overriden when calling encrypt_file)
        :param check_signature: whether to check signature when decrypting file
        (can be overriden when calling decrypt_file)
        :param homedir: store keys in given directory instead of home dir,
        and don't remove that directory on clenaup.
        """
        # FIXME use fixed directory, not tempdir,
        # and persist key across runs when possible
        if homedir:
            self.home = homedir
            self.rmhome = False
        else:
            self.home = tempfile.mkdtemp()
            self.rmhome = True
        self.gpg = gnupg.GPG(gnupghome=self.home)
        self.password = password
        self.sign = sign
        self.check_signature = check_signature
        self.known_keys = set()

    def _import_key(self, data):
        """
        Import key(s) from given data which is an armored encoding of the key.
        Will return hash for the first key imported from that data string.
        """
        ir = self.gpg.import_keys(data)
        if not ir:
            logger.error('Failed to import key:')
            logger.error(ir.stderr)
            raise ValueError('Failed to import key')
        self.known_keys.update(ir.fingerprints)
        return ir.fingerprints[0]

    def import_keys(self, private, public):
        # We want to distinguish
        # between "our" (private) and "their" (public) keys.
        # One way would be to load them from single file
        # and then just be told their hashes.
        # Another way is to load them from different files
        # and automatically determine hashes.
        # For now we will use that second way.
        logger.info('Importing private key...')
        self.private_keyhash = self._import_key(private)
        logger.info('Importing public key...')
        self.public_keyhash = self._import_key(public)
        # we want also "additional" fingerprints if any - XXX do we need this?
        self.known_keys.update(
            k['fingerprint']
            for k in self.gpg.list_keys()
        )
        logger.info('Done importing keys')

    def import_keys_from_s3(self, bucket, private, public):
        s3 = get_s3(bucket)
        self.import_keys(
            s3.fetch(private),
            s3.fetch(public),
        )

    def encrypt_file(self, src, dst, sign=None, armor=False):
        """
        Encrypt given file `src` with public key and write result to `dst`.
        :param sign: whether to also sign file with private key
        in addition to encrypting it with public key
        :param armor: whether to produce ASCII-armored output
        (default is binary)
        """
        if sign is None:
            sign = self.sign  # use default

        logger.info('Will encrypt file %s to %s %s signing and %s armoring',
                    src, dst, 'with' if sign else 'without',
                    'with' if armor else 'without')

        # XXX will it properly use private key for signing?
        with open(src, 'rb') as f:
            ret = self.gpg.encrypt_file(
                f, self.public_keyhash,
                always_trust=True,
                passphrase=self.password,
                sign=sign,
                armor=armor,
                output=dst,
            )
        if not ret.ok:
            logger.error('Failed to encrypt:')
            logger.error(ret.stderr)
            raise ValueError('Encryption failed')

        logger.info('Done encrypting')

    def decrypt_file(self, src, dst, check_signature=None):
        if check_signature is None:
            check_signature = self.check_signature  # use default

        logger.info('Will decrypt file %s to %s %s checking signature',
                    src, dst, 'with' if check_signature else 'without')

        with open(src, 'rb') as f:
            ret = self.gpg.decrypt_file(
                f,
                always_trust=True,
                passphrase=self.password,
                # don't use output arg because we want to check signature
            )
        if not ret.ok:
            logger.error('Failed to decrypt:')
            logger.error(ret.stderr)
            raise ValueError('Decryption failed')

        if check_signature:
            if not ret.signature_id:
                logger.error('File is not signed')
                raise ValueError('File is not signed')
            if ret.fingerprint not in self.known_keys:
                raise ValueError(
                    'Signature fingerprint {} is not known;'
                    'known keys: {}'.format(
                        ret.fingerprint,
                        ', '.join(self.known_keys),
                    ),
                )

        with open(dst, 'wb') as f:
            logger.debug('Writing resulting data...')
            f.write(ret.data)

        logger.info('Decryption done')

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.cleanup()

    def __del__(self):
        if self.home:
            self.cleanup()

    def cleanup(self):
        if self.rmhome:
            assert self.home, 'Already cleaned up?'
            shutil.rmtree(self.home)
        self.home = None


def get_gpg(*args, **kwargs):
    return GpgHelper(*args, **kwargs)
