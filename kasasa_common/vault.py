class VaultSupport(object):
    """
        Expected ENV vars:
            VAULT_APPROLE_PATH:      Required, houses the path in s3 where the app role is stored
            VAULT_APPROLE_SECRET_ID: Required, is the ID to access vault for the lambda
            VAULT_SECRET_KEY:        Required, name of the lambda to access vault
            VAULT_ADDRESS:           Required (if CONSUL_ADDRESS not being used), url for vault
            CONSUL_ADDRESS:          Required (if VAULT_ADDRESS not being used), tells the hvac client where to connect to consul
            VAULT_PROTOCOL:          Optional, defaults to https (what you want)

        OR

            dict(
                VAULT_APPROLE_PATH="value",
                VAULT_APPROLE_SECRET_ID="value",
                VAULT_SECRET_KEY="value",
                VAULT_ADDRESS="value",
                CONSUL_ADDRESS="value"
            )

        Based heavily on work done for seed-lambda project:
            http://git.bvops.net/projects/POC/repos/seed-lambda/browse/src/configuration_reader.py
    """
    def __init__(self, logger, vault_config):
        self.logger = logger
        self.vault_config = vault_config

    def _get_vault_address(self, consul_address, vault_protocol):
        if consul_address is None:
            return None

        try:
            from urllib.parse import urlparse
        except ImportError as ie:
            import urlparse.urlparse as urlparse

        import consul

        parse_result = urlparse(consul_address)
        consul_instance = consul.Consul(host=parse_result.hostname,
                                        port=parse_result.port,
                                        scheme=parse_result.scheme)
        # See [sample response](https://www.consul.io/api/catalog.html#sample-response-3)
        service_result = consul_instance.catalog.service('vault', tag='active', consistency='stale')
        services_details = service_result[1]
        if len(services_details) == 0:
            self.logger.debug("Vault not found.")
            return None
        vault_result = service_result[1][0]
        vault_address = "{protocol}://{address}:{port}".format(protocol=vault_protocol,
                                                               address=vault_result['ServiceAddress'],
                                                               port=vault_result['ServicePort'])
        self.logger.debug("Found vault at {vault_address}".format(vault_address=vault_address))
        return vault_address

    def _get_vault_approle(self, vault_approle_path):
        """
        Retrieves the vault approle from the given vault_approle_path on S3
        and returns the retrieved value.

        Args:
            vault_approle_path: The S3 path to the vault approle.
            e.g. bucket/prefix/to/key.txt

        Returns: The vault approle
        """
        import os
        from kasasa_common import get_s3

        self.logger.debug('Accessing vault approle.')
        # Get local S3 values if available.
        bucket = get_s3(vault_approle_path.replace('s3://', '').split("/")[0])
        key = "/".join(vault_approle_path.replace('s3://', '').split("/")[1:])
        self.logger.debug('bucket: {bucket}; key: {key}'.format(bucket=bucket.bucket, key=key))
        # Get the vault approle object at the bucket and key above.
        vault_approle = bucket.fetch(key)

        self.logger.debug('Successfully read vault approle role_id and secret_id.')

        return vault_approle

    def get_secrets(self):
        """
        Retrieve values stored in vault.

        Args:
            None

        Returns: Map of vault secrets.
        """
        import os
        import hvac
        import sys

        # Vault is available over https in AWS. Therefore,
        # default to https.
        # Set VAULT_PROTOCOL to http for local development.
        vault_protocol = self.vault_config.get('VAULT_PROTOCOL', os.getenv('VAULT_PROTOCOL', 'https'))

        try:
            # get vault address from config dict first, env vars second, or consul last
            # (use `or` to avoid evaluating consul if other options are available)
            vault_addr = (
                self.vault_config.get('VAULT_ADDRESS') or
                os.getenv('VAULT_ADDRESS') or
                self._get_vault_address(
                    self.vault_config.get('CONSUL_ADDRESS') or os.getenv('CONSUL_ADDRESS'),
                    vault_protocol,
                )
            )
            if vault_addr is not None:
                vault_client = hvac.Client(url=vault_addr)
                vault_approle_path = self.vault_config.get('VAULT_APPROLE_PATH') or os.getenv('VAULT_APPROLE_PATH')
                if not vault_approle_path:
                    raise AssertionError("VAULT_APPROLE_PATH value not found")
                vault_approle_secret_id = self.vault_config.get('VAULT_APPROLE_SECRET_ID') or os.getenv('VAULT_APPROLE_SECRET_ID')
                if not vault_approle_secret_id:
                    raise AssertionError("VAULT_APPROLE_SECRET_ID value not found")
                # Allow the caller to override the key to read the secret from
                # for local development.
                vault_secret_key = self.vault_config.get('VAULT_SECRET_KEY') or os.getenv("VAULT_SECRET_KEY")
                if not vault_secret_key:
                    raise AssertionError("VAULT_SECRET_KEY value not found")
                vault_approle = self._get_vault_approle(vault_approle_path)
                # See [Perform the login operation to fetch a new Vault token]
                # (https://www.vaultproject.io/docs/auth/approle.html#perform-the-login-operation-to-fetch-a-new-vault-token-)
                # for response format.
                auth_result = vault_client.auth_approle(vault_approle, vault_approle_secret_id)
                vault_client.token = auth_result['auth']['client_token']
                if not vault_client.is_authenticated():
                    raise AssertionError("vault client is not authenticated")
                # See [Read Secret sample response]
                # (https://www.vaultproject.io/api/secret/generic/index.html#sample-response)
                # for response format.
                read_result = vault_client.read('secret/{vault_secret_key}'.format(vault_secret_key=vault_secret_key))
                vault_values = read_result['data']

                self.logger.debug('Successfully retrieved secrets from {vault_secret_key}.'.format(vault_secret_key=vault_secret_key))
                return vault_values
            else:
                self.logger.debug('Unable to acquire secrets')
                return {}
        except Exception as e:
            self.logger.error(e, exc_info=True)
            raise


def get_vault_secrets(logger=None, vault_config={}):
    """
    Retrieve the values stored in vault for the lambda.

    Args:
        logger: The logger instance to use

    Returns: Map of vault values.

    """
    if not logger:
        import logging
        logger = logging.getLogger('vault')
    return VaultSupport(logger, vault_config).get_secrets()
