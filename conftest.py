# This file is required here for pytest to properly handle --quick option

# FIXME does it work correctly,
# or it actually brings all the stuff from that conftest?
# In the latter case we may want to split that addoption hook impl
# into separate module
from kasasa_common.tests.conftest import pytest_addoption


pytest_plugins = [
    'kasasa_common.tests.moto_helpers',
]
