# Constants
ARTIFACT=kasasa-common
VERSION=3.2.0
ARCHIVE=$(ARTIFACT)-$(VERSION).tar.gz
BUILD_DIR=dist
VIRTUAL_ENVIRONMENT_NAME=venv

guard-%:
    @ if [ "${${*}}" = "" ]; then \
        echo "Environment variable $* not set"; \
        exit 1; \
    fi

clean:
	# start with a clean slate
	rm -rf $(BUILD_DIR)

prepare:requirements.txt
	# Install virtualenv to create isolated Python environment
	# so that dependencies are isolated without polluting the global namespace	
	test -d venv || virtualenv $(VIRTUAL_ENVIRONMENT_NAME)	
	$(VIRTUAL_ENVIRONMENT_NAME)/bin/pip install -r requirements-test.txt
	touch venv/bin/activate

test: prepare
	# PEP 8 Violoations
	#
	$(VIRTUAL_ENVIRONMENT_NAME)/bin/pep8 kasasa_common/*.py --show-source --ignore=E501,E302,E226
	
	# Report code coverage
	$(VIRTUAL_ENVIRONMENT_NAME)/bin/py.test --ignore $(VIRTUAL_ENVIRONMENT_NAME) --cov tests/ --cov-report term-missing --cov-report xml

	# produce coverage.xml for corbertura plugin
	$(VIRTUAL_ENVIRONMENT_NAME)/bin/coverage xml --omit=venv/*
	
build: prepare
	# Build the archive
	# setup.py file outlines the project's package dependencies, version and entry_points and
	# provides us with two dependency declarations:
	# 	install_requires -specifies libraries required to install and uitized in the project
	# 	tests_require -specifies libraries required to test your project
	python setup.py sdist

publish: build
	# take the archive from the build step and shove it to our nexus artifactory
	# which will be picked up by the downstream job dw-deliver-dev->(rundeck-aws-deploy/deliver-dw-python-modules.sh)	
	curl -m 300 \
		-u "$(ORG_GRADLE_PROJECT_nexusUsername):$(ORG_GRADLE_PROJECT_nexusPassword)" \
		-T "./$(BUILD_DIR)/$(ARCHIVE)" \
		"$(ORG_GRADLE_PROJECT_repositoryReleaseUrl)/com/bancvue/$(ARTIFACT)/$(VERSION)/$(ARCHIVE)"
tearDown: publish
	# Look like we are done with what we are supposed to do, now its time to kick our good friend "virtualenv" out.	
	rm -rf ${VIRTUAL_ENVIRONMENT_NAME}

# Ok, Jenkins here is the step definition for you
ci: clean test publish tearDown

# prevent "make" from getting confused by an actual file called test
.PHONY: test
