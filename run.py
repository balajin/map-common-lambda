#!/usr/bin/env python

import argparse
import os.path as op

import click

import common


def log(msg):
    click.secho(msg, fg='green', bold=True)

@click.command(
    help='''
    For input and output files:
    there are 3 ways to specify them.

    1. Pass single infile/outfile option.
    This file will be passed to pig as the only argument.
    2. Pass several files. They will be passed to pig as a list.
    3. Pass several files and prepend each of them with name:

    -i phones=phones.txt

    In such case they will be passed to pig as a dict.

    Input files will be taken from a bucket,
    and output files will be uploaded.
    If filename includes path, that path applies only to the bucket
    but not to pig.
    So filenames should not clash.
    ''')
@click.argument('script')
@click.option('-d', '--download-from', default='data', show_default=True,
              help='Name of the bucket to get source files from.')
@click.option('-u', '--upload-to', default='results', show_default=True,
              help='Name of the bucket to upload results to.')
@click.option('-i', '--infile', multiple=True,
              help='Input file. If used many times then will pass list; '
              'if has format like `-i name=path_to_file` '
              'then will pass dict.')
@click.option('-o', '--outfile', multiple=True,
              help='Output file. Uses same logic as infile.')
@click.option('-k', '--keep-files', is_flag=True,
              help='Don\'t remove temporary directory on exit '
              '(on exception they will always be kept)')
@click.option('-r', '--rotate', is_flag=True,
              help='Rotate all outputs after running pig')
def pig(script, download_from, upload_to, infile, outfile, keep_files, rotate):
    # prepare args
    def preprocess_filelist(src):
        # Return list of filenames and object to pass to pig

        if not src:  # nothing passed?
            scriptbase = op.basename(script)
            sname, _ = op.splitext(scriptbase)
            return [sname + '.txt'], None  # inferring from scriptname

        if all('=' in f for f in src):
            # dict required
            return [
                f.split('=', 1)[1] for f in src
            ], {
                k: op.basename(v)
                for k, v in (
                    f.split('=', 1)
                    for f in src
                )
            }
        if len(src) == 1:
            # just one item => pass as a string
            return src, op.basename(src[0])
        # pass list as is
        return src, [op.basename(f) for f in src]

    innames, infile = preprocess_filelist(infile)
    outnames, outfile = preprocess_filelist(outfile)

    # we want to prepare them early
    # to check for missing buckets
    source = common.get_s3(download_from)
    target = common.get_s3(upload_to)

    with common.get_pig(keep_files=keep_files) as pig:
        try:
            pig.ensure_ready()

            # download sources
            log('Downloading sources from {!r}...'.format(download_from))
            for f in innames:
                source.download(f, pig.INDIR)

            log('Runnign pig script {!r}...'.format(script))
            pig.run(script, infile, outfile)

            if rotate:
                log('Rotating directories...')
                pig.rotate_directories(*outnames)

            # upload results
            log('Uploading results to {!r}...'.format(upload_to))
            for f in outnames:
                target.upload(op.join(pig.OUTDIR, f), overwrite=True)

            log('Done!')
        except Exception as e:
            click.secho('Something went wrong! Traceback follows.',
                        fg='red', bold=True)
            # on exception the user will want to see some logs.
            pig.keep_files = True
            raise
        finally:
            if pig.keep_files:
                log('All temporary files kept in place, find them in {}'.format(
                    pig.directory))

if __name__ == '__main__':
    pig()
